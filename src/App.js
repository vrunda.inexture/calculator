import React, { Fragment } from 'react';
import CalcRow from './components/CalcRow';
import './App.css';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      input: '',
      numbers: [9, 8, 7, 6, 5, 4, 3, 2, 1, 0],
      result: null,
      error: '',
    };
  }
  render() {
    const { numbers, input, result, error } = this.state;

    //randomizes the numbers
    const randomize = () => {
      let currentIndex = numbers.length,
        randomIndex;

      // While there remain elements to shuffle...
      while (currentIndex != 0) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [numbers[currentIndex], numbers[randomIndex]] = [
          numbers[randomIndex],
          numbers[currentIndex],
        ];
      }
      this.setState({ numbers });
    };

    // calculation
    const calculate = (num) => {
      switch (num) {
        case 'CE':
          if (error || result !== null) {
            clearState();
          } else {
            // backspace
            this.setState({
              input: input.slice(0, -1),
              result: null,
              error: '',
            });
          }
          break;
        case 'C':
          clearState();
          break;
        case '=':
          if (input) {
            try {
              this.setState({
                result: eval(input),
                error: '',
              });
            } catch (err) {
              this.setState({
                error: 'Error',
                result: null,
              });
            }
          }
          break;
        default:
          this.setState({
            input: `${input}${num}`,
          });
      }
    };

    // clears the state
    const clearState = () => {
      this.setState({
        input: '',
        result: null,
        error: '',
      });
    };

    // onclick handler(general function)
    const onclickHandler = (num) => {
      if (!isNaN(num)) {
        randomize();
      }
      calculate(num);

      // new iteration
      if (num !== '=' && (result !== null || error)) {
        clearState();
      }
    };

    // what to display
    let output;
    if (result !== null) {
      output = result;
    } else if (error) {
      output = error;
    } else {
      output = input;
    }

    return (
      <Fragment>
        <h1> Randomize Calculator </h1>
        <hr />
        <table className='table table-bordered'>
          <thead>
            <tr>
              <th colSpan='4'>{output}</th>
            </tr>
          </thead>
          <tbody>
            <CalcRow
              onclickHandler={onclickHandler}
              values={['(', 'CE', ')', 'C']}
            />

            <CalcRow
              onclickHandler={onclickHandler}
              values={[numbers[0], numbers[1], numbers[2], '+']}
            />

            <CalcRow
              onclickHandler={onclickHandler}
              values={[numbers[3], numbers[4], numbers[5], '-']}
            />
            <CalcRow
              onclickHandler={onclickHandler}
              values={[numbers[6], numbers[7], numbers[8], '*']}
            />
            <CalcRow
              onclickHandler={onclickHandler}
              values={['.', numbers[9], '=', '/']}
            />
          </tbody>
        </table>
      </Fragment>
    );
  }
}

export default App;
