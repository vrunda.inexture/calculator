import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Fragment } from 'react/cjs/react.production.min';

export class CalcRow extends Component {
  static propTypes = {
    onclickHandler: PropTypes.func.isRequired,
    values: PropTypes.array.isRequired,
  };

  render() {
    const { onclickHandler, values } = this.props;
    return (
      <Fragment>
        <tr>
          {values.map((value, index) => (
            <td key={index} onClick={() => onclickHandler(value)}>
              {value}
            </td>
          ))}
        </tr>
      </Fragment>
    );
  }
}

export default CalcRow;
